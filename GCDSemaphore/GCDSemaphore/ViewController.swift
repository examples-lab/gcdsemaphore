//
//  ViewController.swift
//  GCDSemaphore
//
//  Created by 金鑫 on 2020/6/9.
//  Copyright © 2020 金鑫. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        testSemaphone()
    }

}

extension ViewController {
    
    func testSemaphone() {
        let group = DispatchGroup()
        
        DispatchQueue.global().async(group: group, execute: DispatchWorkItem(block: {
            let semaphore = DispatchSemaphore(value: 0)
            
            DispatchQueue.global().async {
                Thread.sleep(forTimeInterval: 2)
                print("完成网络请求A1\(Thread.current)")
                semaphore.signal()
            }
            semaphore.wait()
            print("完成任务A1\(Thread.current)")
        }))
        
        DispatchQueue.global().async(group: group, execute: DispatchWorkItem(block: {
            let semaphore = DispatchSemaphore(value: 0)
            
            DispatchQueue.global().async {
                Thread.sleep(forTimeInterval: 2)
                print("完成网络请求A2\(Thread.current)")
                semaphore.signal()
            }
            semaphore.wait()
            print("完成任务A2\(Thread.current)")
        }))
        
        DispatchQueue.global().async(group: group, execute: DispatchWorkItem(block: {
            let semaphore = DispatchSemaphore(value: 0)
            
            DispatchQueue.global().async {
                Thread.sleep(forTimeInterval: 2)
                print("完成网络请求A3\(Thread.current)")
                semaphore.signal()
            }
            semaphore.wait()
            print("完成任务A3\(Thread.current)")
        }))
        
        group.notify(queue: DispatchQueue.main) {
            print("全部完成\(Thread.current)")
        }
        
        print("异步任务不会阻塞线程 Semaphore")
    }
}
